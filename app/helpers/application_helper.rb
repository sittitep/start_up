module ApplicationHelper
  def vertical_button(links)
    html = ""
    links.each do |link|
      html += "<a href=#{link[0]} class=#{link[2]}><div class='text'>#{link[1]}</div></a>"
    end
    return html.html_safe
  end

  def mobile_menu(links,sub_menu)
    sub_menu_html = ""
    sub_menu.each_with_index do |link,index|
      style = link[2] + " mobile-sub-menu"
      sub_menu_html += "<a href=#{link[0]} class='#{style}'>#{link[1].upcase}</a>"
    end
    html = ""
    links.each_with_index do |link,index|
      html += "<a href=#{link[0]} class=#{@active_menu == link[1] ? "active" : link[2]}>#{link[1].upcase}</a>"
      if @active_menu == link[1]
        html += sub_menu_html
      end
    end
    return html.html_safe
  end

  def menu(links)
    html = ""
    links.each_with_index do |link,index|
      html += "<a href=#{link[0]} class=#{@active_menu == link[1] ? "active" : link[2]}>#{link[1].upcase}</a>"
    end
    return html.html_safe
  end

  def cover_image(options = {})
    return image_tag(options[:resource].images.first.file.url(options[:size])) if options[:resource].images.present?
  end

  def on_hand(resource)
    if resource.class.name == "Product"
      amt = 0
      resource.variants.present? ? amt = resource.variants.map(&:on_hand).compact!.inject{|sum,x| sum + x } : amt = resource.on_hand
      return "#{amt}"
    else
      return "#{resource.on_hand }"
    end
  end

  def money(amount = 0)
    return number_to_currency(amount, precision: 2, locale: :th)  
  end
end
