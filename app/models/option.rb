class Option < ActiveRecord::Base
  has_many :values, as: :valuable
  has_many :product_collections, as: :collectable
  has_many :option_choices

  belongs_to :product

  accepts_nested_attributes_for :option_choices, :allow_destroy => true

  validates :name, presence: true
  validates :name, uniqueness: true
end
