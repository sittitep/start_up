class Variant < ActiveRecord::Base
  belongs_to :product
  has_many :images, as: :imageable
  has_many :values, as: :originator
  validates :name, presence: true

  accepts_nested_attributes_for :images, :allow_destroy => true
  accepts_nested_attributes_for :values, :allow_destroy => true

  after_save :generate_option_values

  def initialize(attributes={})
    attr_with_defaults = {master_price: 0.00,
                          on_hand: 0,
      }.merge(attributes)
    super(attr_with_defaults)
  end

  def generate_option_values
    collections = self.product.product_collections.where(collectable_type: "Option")
    collections.each do |collection|
      self.values.create(valuable_id: collection.collectable_id, valuable_type: collection.collectable_type)
    end
  end
end
