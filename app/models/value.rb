class Value < ActiveRecord::Base
  belongs_to :valuable, polymorphic: true

  def valuable_name
    valuable_type.constantize.find_by_id(valuable_id).name
  end

  def self.remove_value(originator,valuable)
    value = Value.find_by_originator_id_and_originator_type_and_valuable_id_and_valuable_type(originator.id,originator.class.to_s,valuable.id,valuable.class.to_s)
    value.destroy if value
  end
end
