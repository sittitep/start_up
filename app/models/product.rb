class Product < ActiveRecord::Base
  has_many :options
  has_many :variants
  has_many :images, as: :imageable
  has_many :values, as: :originator
  has_many :product_collections
  attr_accessor :option_ids, :property_ids

  accepts_nested_attributes_for :images, :allow_destroy => true
  accepts_nested_attributes_for :values, :allow_destroy => true
  accepts_nested_attributes_for :variants, :allow_destroy => true

  validates :name, presence: true
  validates :name, uniqueness: true
  validates :sku, uniqueness: true
  validates :on_hand, numericality: true
  validates :master_price, numericality: true

  def initialize(attributes={})
    attr_with_defaults = {master_price: 0.00,
                          on_hand: 0,
      }.merge(attributes)
    super(attr_with_defaults)
  end

  def has_variants?
    self.variants.present?
  end

  def option_ids
    product_collections.where(collectable_type: "Option").pluck(:collectable_id)
  end

  def property_ids
    product_collections.where(collectable_type: "Property").pluck(:collectable_id)
  end

  def update_collection(hash = {})
    if hash[:ids].present?
      ids = hash[:ids].map{|x| x.to_i if x.present?}.compact!
      ids.each do |id|
        unless product_collections.find_by_collectable_id_and_collectable_type(id,hash[:type])
          product_collections.create(collectable_id: id, collectable_type: hash[:type])
          # do not create value for product if type is option
          values.create(valuable_id: id, valuable_type: hash[:type]) unless hash[:type] == "Option"
          if variants.present? && hash[:type] == "Option"
            variants.each do |v|
              v.values.create(valuable_id: id, valuable_type: hash[:type])
            end
          end
        end
      end

      #find unused ids
      unused_ids =  product_collections.where(collectable_type: hash[:type]).pluck(:collectable_id) - ids
      
      #destroy them if exist
      if unused_ids.present?
        ProductCollection.where(collectable_id: unused_ids, collectable_type: hash[:type], product_id: self.id).each do |c|
          c.destroy
          Value.remove_value(c.product,c.collectable)
          if variants.present? && hash[:type] == "Option"
            variants.each do |v|
              Value.remove_value(v,c.collectable)
            end
          end
        end
      end

    end   
  end

end
