class Property < ActiveRecord::Base
  has_many :values, as: :valuable
  has_many :product_collections, as: :collectable

  validates :name, presence: true
  validates :name, uniqueness: true
end
