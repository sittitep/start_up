class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :empty_links!
  before_filter :menu

  def empty_links!
    @menu = Array.new
    @sub_menu = Array.new
    @head_links = Array.new
    @foot_links = Array.new
  end

  def menu
    @menu << [root_path, t(:dashboard), "none"]
    @menu << [products_path, t(:products), "none"]
  end
end
