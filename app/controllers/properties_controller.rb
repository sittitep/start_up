class PropertiesController < ApplicationController
  before_filter :get_property, only: [:edit,:update,:destroy]
  before_filter :set_active_menu

  def set_active_menu
    @active_menu = "products"
    @sub_menu << [options_path, t(:options), ""]
    @sub_menu << [properties_path, t(:properties), "active"]
  end

  def index
    @properties = Property.all
    @property = Property.new
  end
  def create
    @property = Property.new property_params
    @property.save

    redirect_to properties_path
  end
  def edit
    
  end
  def update
    @property.update_attributes property_params
    redirect_to properties_path
  end
  def destroy
    @property.product_collections.each do |product_collection|
      product_collection.destroy
    end
    @property.values.each do |value|
      value.destroy
    end
    @property.destroy
    redirect_to properties_path
  end

  private
    def get_property
      @property = Property.find(params[:id])
    end

    def property_params
      params.require(:property).permit!
    end
end
