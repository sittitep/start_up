class VariantsController < ApplicationController
  before_filter :get_variant, only: [:show,:edit,:update,:destroy]
  before_filter :get_product, only: [:index,:new,:destroy,:edit]
  before_filter :set_active_menu

  def set_active_menu
    @active_menu = "products"
    @sub_menu << [options_path, t(:options), ""]
    @sub_menu << [properties_path, t(:properties), ""]
  end

  def create
    @variant = Variant.new variant_params

    if @variant.save
      redirect_to edit_variant_path(@variant)
    else
      redirect_to variants_product_path(@variant.product_id)
    end
  end

  def edit
    @head_links << [variants_product_path(@product), t(:variants),"button"]
  end

  def update
    @variant.update_attributes variant_params
    redirect_to edit_variant_path(@variant)
  end

  def destroy
    @variant.destroy
    redirect_to variants_product_path(@product)
  end

  private

    def get_product
      @product = Product.find_by_id(params[:product_id]) || @variant.product
    end

    def get_variant
      @variant = Variant.find(params[:id])
    end

    def variant_params
      params.require(:variant).permit!
    end
end
