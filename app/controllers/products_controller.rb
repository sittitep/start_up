class ProductsController < ApplicationController
  before_filter :get_product, only: [:show,:edit,:update,:destroy,:options,:variants,:properties,:images]
  before_filter :set_active_menu
  before_filter :set_to_product, only: [:images,:properties]

  def set_active_menu
    @active_menu = "products"
    @sub_menu << [options_path, t(:options), ""]
    @sub_menu << [properties_path, t(:properties), ""]
  end

  def set_to_product
    @head_links << [edit_product_path, @product.name, "button"]
  end

  def index
    @q = Product.search(params[:q])
    @products = @q.result(distinct: true)
    @head_links << [new_product_path, t(:new), "button"]
  end

  def show
    @head_links << [edit_product_path(@product),"EDIT PRODUCT","yellow"]
    @foot_links << [products_path,"BACK TO PRODUCTS","grey"]
  end

  def new
    @product = Product.new
    @foot_links << [products_path,"BACK TO PRODUCTS","grey"]
  end

  def create
    @product = Product.new product_params

    if @product.save
      redirect_to edit_product_path(@product)
    else
      redirect_to new_product_path
    end
  end

  def edit
    @head_links << [variants_product_path(@product), t(:variants),"button"]
    @head_links << [properties_product_path, t(:properties),"button"]
    @head_links << [images_product_path(@product), t(:images),"button"]
  end

  def update
    @product.update_attributes product_params
     
    @product.update_collection({type: "Option", ids: params[:product][:option_ids]})
    @product.update_collection({type: "Property", ids: params[:product][:property_ids]})

    redirect_to edit_product_path(@product)
  end

  def destroy
    @product.destroy
    redirect_to products_path
  end

  def variants
    @variants = @product.variants
    @variant = Variant.new(product_id: @product.id)
    @head_links << [edit_product_path, @product.name, "button"]
  end

  def properties
    
  end

  def images
    
  end

  private
    def disable_form
      @disable_form = true
    end

    def get_product
      @product = Product.find(params[:id])
    end
    def product_params
      params.require(:product).permit!
    end
end
