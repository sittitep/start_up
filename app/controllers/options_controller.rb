class OptionsController < ApplicationController
  before_filter :get_option, only: [:edit,:update,:destroy]
  before_filter :set_active_menu

  def set_active_menu
    @active_menu = "products"
    @sub_menu << [options_path, t(:options), "active"]
    @sub_menu << [properties_path, t(:properties), ""]
  end

  def index
    @options = Option.all
    @option = Option.new
  end

  def create
    @option = Option.new option_params
    @option.save

    redirect_to options_path
  end

  def edit
    @foot_links << [options_path(product_id: @option.product_id),"BACK TO OPTIONS","grey"]
  end

  def update
    @option.update_attributes option_params
    redirect_to options_path
  end

  def destroy

    @option.product_collections.each do |product_collection|
      product_collection.destroy
    end

    @option.values.each do |value|
      value.destroy
    end

    @option.option_choices.each do |option_choice|
      option_choice.destroy
    end

    @option.destroy
    redirect_to options_path
  end

  private

    def get_option
      @option = Option.find(params[:id])
    end

    def option_params
      params.require(:option).permit!
    end
end
