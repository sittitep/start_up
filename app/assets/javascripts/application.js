// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery_nested_form
//= require jquery.event.swipe
//= require chosen-jquery
//= require_tree .


$(document).ready(function(){
  $(".resource-cover").on("click", function(){
    current = $(this).find("img.show");
    next = $(this).find("img.show").next();
    if(next.size() < 1){
      next = $(this).find("img").first()
    };
    next.removeClass("hide").addClass("show");
    current.removeClass("show").addClass("hide");
  });
  $("#toggle").on("click", function(){
    var menu = $("#mobile-main-menu")
    if (menu.hasClass("hide")){
      menu.removeClass("hide")
    }else{
      menu.addClass("hide")
    }
  });
  $('.chosen-select').chosen({
    allow_single_deselect: true,
    no_results_text: 'No results matched',
    width: '100%'
  });
});