class CreateOptionChoices < ActiveRecord::Migration
  def change
    create_table :option_choices do |t|
      t.string :value
      t.integer :option_id

      t.timestamps
    end
  end
end
