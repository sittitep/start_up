class CreateVariants < ActiveRecord::Migration
  def change
    create_table :variants do |t|
      t.string :sku
      t.string :name
      t.string :label
      t.text :description
      t.decimal :master_price, :precision => 10, :scale => 2
      t.integer :on_hand
      t.integer :product_id

      t.timestamps
    end
  end
end
