class CreateProductCollections < ActiveRecord::Migration
  def change
    create_table :product_collections do |t|
      t.integer :collectable_id
      t.string :collectable_type
      t.integer :product_id

      t.timestamps
    end
  end
end
