class CreateValues < ActiveRecord::Migration
  def change
    create_table :values do |t|
      t.integer :valuable_id
      t.string :valuable_type
      t.integer :originator_id
      t.string :originator_type
      t.string :value

      t.timestamps
    end
  end
end
