# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140131182523) do

  create_table "images", force: true do |t|
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "option_choices", force: true do |t|
    t.string   "value"
    t.integer  "option_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "options", force: true do |t|
    t.string   "name"
    t.string   "label"
    t.string   "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_collections", force: true do |t|
    t.integer  "collectable_id"
    t.string   "collectable_type"
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", force: true do |t|
    t.string   "sku"
    t.string   "name"
    t.string   "label"
    t.text     "description"
    t.decimal  "master_price", precision: 10, scale: 2
    t.integer  "on_hand"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "properties", force: true do |t|
    t.string   "name"
    t.integer  "shop_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "values", force: true do |t|
    t.integer  "valuable_id"
    t.string   "valuable_type"
    t.integer  "originator_id"
    t.string   "originator_type"
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "variants", force: true do |t|
    t.string   "sku"
    t.string   "name"
    t.string   "label"
    t.text     "description"
    t.decimal  "master_price", precision: 10, scale: 2
    t.integer  "on_hand"
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
